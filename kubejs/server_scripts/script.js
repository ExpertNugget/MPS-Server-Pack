// priority: 0

settings.logAddedRecipes = true
settings.logRemovedRecipes = true
settings.logSkippedRecipes = false
settings.logErroringRecipes = true

console.info('Hello, World! (You will see this line every time server resources reload)')

onEvent('recipes', event => {
	// Change recipes here
  event.shaped('2x immersiveengineering:radiator', [
    'SCS',
    'CWC',
    'SCS'
  ], {
    S: '#forge:ingots/steel',
	C: '#forge:ingots/copper',
	W: 'minecraft:water_bucket'
  })
}) //This was deleted -Chief

onEvent('item.tags', event => {
	// Get the #forge:cobblestone tag collection and add Diamond Ore to it
	// event.get('forge:cobblestone').add('minecraft:diamond_ore')

	// Get the #forge:cobblestone tag collection and remove Mossy Cobblestone from it
	// event.get('forge:cobblestone').remove('minecraft:mossy_cobblestone')

	event.get('forge:stone').add('astralsorcery:marble_raw'),
	event.get('botania:semi_disposable').add('astralsorcery:marble_raw'),
	event.get('quark:stone_tool_materials').add('astralsorcery:marble_raw'),
	event.get('forge:gems').add('mekanism:fluorite_gem')
})